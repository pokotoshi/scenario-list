class ScreateController < ApplicationController

  def top
    @text = params["text"]
    @title = params["title"]
    @other = params["other"]
    @names = params["name"]
    @multi_names = params["multi-name"]
    @colors = params["color"]
    @sex = params["sex"]
    @multi_sex = params["multi-sex"]
    @padding = params["padding"]
    @datas = params["data"]
    @multi_datas = params["multi-data"]
    @style_change = params["style_change"]
    @multi_check = {}
    @multi_check = params["multi"]
    @name_cnt = {}

    if @multi_check.blank?
      @multi_check = nil
    end
    if @names.present?
      @names.each{|n|
        if multi_name_check(@multi_names, n)
          @multi_names[n.first].each{|m|
            if m[1].present?
              @name_cnt.store("#{n.first}-#{m.first}" , 0)
            end
          }
        end
        if n[1].blank?
          @names.delete(n.first)
          @sex.delete(n.first)
          @colors.delete(n.first)
          @datas.delete(n.first)
        else
          @name_cnt.store(n.first , 0)
        end
      }
    end

    if @text.present?
      @html_text = create()
    end
  end

  def create()
    men = 0
    women = 0
    humon = 0
    table_text = ""

    #比率
    @sex.each{|s|
      case s[1]
      when "men"
        men += 1
      when "women"
        women += 1
      when "humon"
        humon += 1
      end
    }

    table_text = "<html>\n"

    if @title != ""
      table_text = "<title>#{@title}</title>\n"
    end

    #スタイル指定
    if @style_change.present? || @padding.to_i != 0
      table_text += "\n<style>\n"
    end

    #テーブルレイアウト
    if @style_change.present?
      table_text += "table{ border: 0px; border-right: 1px dotted #DCDCDC; border-collapse: collapse; border-spacing: 0px;}\n"
      table_text += "td{ border-right: 0px; border-left: 0px; border-bottom: 1px solid #A9A9A9;}\n"
      table_text += "td:nth-of-type(1){ background-color: #DCDCDC;border-right: 1px solid #A9A9A9;}"
      table_text += "td:nth-of-type(2){ background-color: #F5F5F5;border-right: 1px solid #A9A9A9;}\n"
    end
    if @padding.to_i != 0
      table_text += "td{padding:#{@padding.to_i}px;}\n"
    end

    #文字色
    @colors.each{|c|
      if @colors[c.first] =~ /#000000/
        @colors.delete(c.first)
      else
        table_text += ".color-#{color_class(c.first)}{\n\tcolor:#{c[1]};\n}\n"
      end
    }

    if @style_change.present? || @padding.to_i != 0
      table_text += "</style>\n\n"
    end

    table_text += "<body>\n"

    if @title != ""
      table_text += "<br>\n<br>\n<center style='font-size:33px;'>#{@title}</center>\n"+ ("<br>\n" * 5 )
    end

    if @other != ""
      table_text += "#{@other.gsub(/(\r\n|\r|\n)/, "<br>\n")}" + ("<br>\n" * 4)
    end

    #役一覧
    table_text += "<strong>&#9794;#{men} &#9792;#{women} 不問#{humon} 計#{men+women+humon}</strong><br>\n<br>\n<br>\n<br>\n登場人物<small>(総セリフ数：(serif-sum))</small><br>\n<br>\n<br>\n"
    @names.each{|n|
      table_text += "<strong class='color-#{color_class(n.first)}'>#{n[1]}</strong>（#{sex_icon(@sex[n.first])}）<small>(セリフ数：(serif-#{n.first}))</small>"
      if @multi_check.present?
        if @multi_check[n.first].present?
          if multi_name_check(@multi_names,n)
            table_text += "<br>\n"
          else
            table_text += "（他の役と被り）<br>\n"
          end
        else
          table_text += "<br>\n"
        end

        if @datas[n.first].present?
          table_text += @datas[n.first] + "<br>\n"
        else
          table_text += "<br>\n"
        end

        if multi_name_check(@multi_names,n)
          table_text += "<div style='margin-left:10px;'>"
          @multi_names[n.first].each{|m_n|
            if m_n[1].present?
              table_text += "<br>\n<span class='color-#{color_class(n.first)}'>#{m_n[1]}</span>（#{sex_icon(@multi_sex[n.first][m_n.first])}）<small>(セリフ数：(serif_#{n.first}_#{m_n.first}))</small>（<span class='color-#{color_class(n.first)}'>#{n[1]}</span>と被り）<br>\n"
              if @multi_datas[n.first].present?
                table_text += "#{@multi_datas[n.first][m_n.first]}<br>\n"
              end
            else
              next
            end
          }

          table_text += "</div>"

        end

        table_text += "<br>\n"

      else
        table_text += "<br>\n"
        if @datas[n.first].present?
          table_text += @datas[n.first] + "<br>\n<br>\n"
        else
          table_text += "<br>\n"
        end
      end
    }

    #配役入力欄作成
    table_text += "<textarea cols='50' rows='#{@names.length + 2}' name='haiyaku'>"
    @names.each{|n|
      table_text += "#{n[1]}(#{sex_icon(@sex[n.first],true)})"
      if @multi_check.present? && @multi_check[n.first].present?
        if multi_name_check(@multi_names, n)
          @multi_names[n.first].each{|m_n|
            table_text += "&#{m_n[1]}(#{sex_icon(@multi_sex[n.first][m_n.first],true)})"
          }
        else
          table_text += "(被り)"
        end
      end
      table_text += "：\n"
    }
    table_text += "</textarea><br>\n<br>\n"

    #テキストを名前と文章に分割
    lines = []
    @text.lines{|line|
      line = line.gsub(/\R/, "")
      if line =~ /\t/
        lines << line.sub(/\"/, "").split("\t")
      else
        lines.last[1] += "<br>" + line.sub(/\"/, "")
      end
    }

    #本文テーブル作成
    line_cnt = 1
    check_flag = false

    table_text += "<table border='1'>\n"

    lines.each{|line|
      name = line[0]
      text = line[1]
      table_text += "\t<tr>\n"

      #セリフ番号
      table_text += "\t\t<td>#{sprintf("%03d",line_cnt)}</td>\n"

      #名前が空白だった場合はト書き
      if name.blank?
        table_text += "\t\t<td></td>\n"
        table_text += "\t\t<td><span style='font-size:14px;font-style:oblique'>#{text}</span></td>\n"
        table_text += "\t</tr>\n"
        next
      end

      #名前の色付け
      @names.each{|n|
        if @multi_check.present? && multi_name_check(@multi_names, n) && @multi_check[n.first].present?
          @multi_names[n.first].each{|m_n|
            if name =~ /(#{m_n[1]}|#{m_n[1]}M|#{m_n[1]}N)/ && m_n[1].present?
              table_text += "\t\t<td><span class='color-#{color_class(n.first)}'>#{name}</span></td>\n"
              table_text += "\t\t<td><span class='color-#{color_class(n.first)}'>#{text}</span></td>\n"
              table_text += "\t</tr>\n"
              @name_cnt["#{n.first}-#{m_n.first}"] += 1
              check_flag = true
              break
            end
          }
        end

        if check_flag
          break
        end

        if name =~ /(#{n[1]}|#{n[1]}M|#{n[1]}N)/
          table_text += "\t\t<td><span class='color-#{color_class(n.first)}'>#{name}</span></td>\n"
          table_text += "\t\t<td><span class='color-#{color_class(n.first)}'>#{text}</span></td>\n"
          table_text += "\t</tr>\n"
          @name_cnt[n.first] += 1
          check_flag = true
          break
        end
      }
      
      unless check_flag
        table_text += "\t\t<td>#{name}</td>\n"
        table_text += "\t\t<td>#{text}</td>\n"
        table_text += "\t</tr>\n"
      end

      check_flag = false
      line_cnt += 1
    }
    table_text += "</table><br>\nEND\n"
    table_text += "</body>\n</html>"

    #総セリフ数
    table_text = table_text.gsub(/\(serif-sum\)/, (line_cnt - 1).to_s)

    #役ごとのセリフ数
    @names.each{|n|
        if multi_name_check(@multi_names, n)
          @multi_names[n.first].each{|m|
            table_text = table_text.gsub(/\(serif_#{n.first}_#{m.first}\)/, @name_cnt["#{n.first}-#{m.first}"].to_s)
          }
        end
      table_text = table_text.gsub(/\(serif-#{n.first}\)/, @name_cnt[n.first].to_s)
    }

    return table_text
  end

  #性別の記号を取得
  def sex_icon(sex,list = false)
    case sex
    when "men"
      return "&#9794;"
    when "women"
      return "&#9792;"
    when "humon"
      if list == true
        return "&#9794;・&#9792;"
      else
        return "不問"
      end
    end
  end


  #デフォルトの色ではわかりやすいクラス名にする
  def color_class(color)
    case @colors[color]
    when "#000000"
      return "black"
    when "#ff0000"
      return "red"
    when "#0000ff"
      return "blue"
    when "#006600"
      return "green"
    when "#7e0021"
      return "brown"
    when "#9933ff"
      return "purple"
    when "#daa520"
      return "yellow"
    else
      return color
    end
  end

  def preview()
    @html = params["html"]
    render layout: "screate_preview_layout"
  end

  #被り役があるかのチェック
  def multi_name_check(arr,n)
    if arr
      if arr[n.first].present?
        if arr[n.first]["0"].present?
          return true
        end
      end
    end
    return false
  end

end
